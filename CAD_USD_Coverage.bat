

REM CAD Coverage

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\CAD\ -t zip -logfile CAD_Coverage


O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a Prices_DB O:\Prodman\Dev\WFI\Scripts\Mysql\CAD_Coverage\CAD_Coverage.sql O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\CAD\PreLDateSuf.txt :_CAD_Coverage CAD_Coverage

O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a Prices_DB O:\Prodman\Dev\WFI\Scripts\Mysql\CAD_Coverage\CAD_Coverage.sql O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\CAD\PreLDateSuf.txt :_CAD_Coverage CAD_Coverage

rem O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a Prices_DB O:\Prodman\Dev\WFI\Scripts\Mysql\CAD_Coverage\CAD_Coverage_Ref.sql O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\CAD\PreLDateSuf.txt :_CAD_Coverage_Ref CAD_Coverage_Ref

O:\AUTO\Scripts\vbs\FixedIncome\newcopyrenamefileAL.vbs O:\Datafeed\Debt\902\ YYMMDD .902 O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\CAD\ YYYYMMDD LOOKUP.txt

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a val -s O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\CAD\ -t txt -n 2 -logfile CAD_Coverage -alert y

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a zip -s O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\CAD\ -d YYYYMMDD_CAD_Coverage -t txt -logfile CAD_Coverage

O:\AUTO\Scripts\vbs\FixedIncome\ftp.vbs O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\CAD\ /Bespoke/CAD_Coverage/ .zip both Upload CAD_Coverage



REM USD Coverage

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\USD -t zip -logfile USD_Coverage

O:\AUTO\Scripts\vbs\FixedIncome\Prod.vbs a Prices_DB O:\Prodman\Dev\WFI\Scripts\Mysql\USD_Coverage\USD_Coverage_Ref.sql O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\USD\PreLDateSuf.txt :_USD_Coverage_Ref USD_Coverage_Ref

O:\AUTO\Scripts\vbs\FixedIncome\newcopyrenamefileAL.vbs O:\Datafeed\Debt\902\ YYMMDD .902 O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\USD\ YYYYMMDD LOOKUP.txt

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a val -s O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\USD\ -t txt -n 2 -logfile USD_Coverage -alert y

powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a zip -s O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\USD\ -d YYYYMMDD_USD_Coverage -t txt -logfile USD_Coverage

O:\AUTO\Scripts\vbs\FixedIncome\ftp.vbs O:\Prodman\Dev\WFI\Feeds\CAD_USD_Coverage\Output\USD\ /Bespoke/USD_Coverage/ .zip both Upload USD_Coverage
